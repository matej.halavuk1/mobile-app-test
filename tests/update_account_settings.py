from appium import webdriver
from configparser import ConfigParser
import selenium.common.exceptions
import json

if __name__ == "__main__":

    # initialize web driver
    def start_web_driver(desired_cap):
        return webdriver.Remote("http://localhost:4723/wd/hub", desired_cap)

    # read config file of ui elements
    def read_ui_elements_file():
        ui_elements_object = ConfigParser()
        ui_elements_object.read("../config/parser/ui_elements.ini")
        return ui_elements_object


    # read config file of credentials
    def read_credentials_file():
        credentials_object = ConfigParser()
        credentials_object.read("../config/app_credentials.ini")
        return credentials_object


    # read config file of android connection
    def read_android_connection_file():
        with open("../config/android_connection.json") as f:
            data = json.load(f)
        return data


    def set_timeout(driver, timeout):
        driver.wait_activity(driver, timeout)


    # create config_object for ui elements
    config_object = read_ui_elements_file()

    # create config_object for credentials
    credentials_object = read_credentials_file()

    # create config_object for android_connection
    android_object = read_android_connection_file()

    # set data for appium to connect to i.e application location on main computer, connected android phone name
    desired_cap = android_object

    # credentials for app login
    username = credentials_object["DATA"]["username"]
    password = credentials_object["DATA"]["password"]

    # list of xpath's & ids of elements used in testing
    username_id = config_object["DATA"]["username_id"]
    password_id = config_object["DATA"]["password_id"]
    login_button_id = config_object["DATA"]["login_button_id"]
    first_agree = config_object["DATA"]["first_agree"]
    second_agree = config_object["DATA"]["second_agree"]
    first_continue = config_object["DATA"]["first_continue"]
    terms_of_use_xpath = config_object["DATA"]["terms_of_use_xpath"]
    privacy_policy_xpath = config_object["DATA"]["privacy_policy_xpath"]
    first_continue_button_xpath = config_object["DATA"]["first_continue_button_xpath"]
    location_xpath = config_object["DATA"]["location_xpath"]
    health_data_xpath = config_object["DATA"]["health_data_xpath"]
    health_data_label_xpath = config_object["DATA"]["health_data_label_xpath"]
    element_to_scroll_to_xpath = config_object["DATA"]["element_to_scroll_to_xpath"]
    notifications_xpath = config_object["DATA"]["notifications_xpath"]
    data_usage_xpath = config_object["DATA"]["data_usage_xpath"]
    second_continue_button_xpath = config_object["DATA"]["second_continue_button_xpath"]
    i_get_it_button_xpath = config_object["DATA"]["i_get_it_button_xpath"]
    skip_pairing_button_xpath = config_object["DATA"]["skip_pairing_button_xpath"]
    first_pop_up_xpath = config_object["DATA"]["first_pop_up_xpath"]
    second_pop_up_xpath = config_object["DATA"]["second_pop_up_xpath"]
    settings_id = config_object["DATA"]["settings_id"]
    personal_settings_xpath = config_object["DATA"]["personal_settings_xpath"]
    first_name_field_xpath = config_object["DATA"]["first_name_field_xpath"]
    back_button_id = config_object["DATA"]["back_button_id"]
    changed_name_id = config_object["DATA"]["changed_name_id"]

    # wanted first name value update
    wanted_name = "TEST"

    # initialize testing value for name of logged in user
    test_name = wanted_name + ' ' + 'HALAVUK'

    # initialize webdriver
    driver = start_web_driver(desired_cap)


    # wait for 5 sec on main page for elements to load
    set_timeout(driver, 5)

    # find and type in username field, also an example of using try catch
    try:
        e = driver.find_element_by_id(username_id)
    except selenium.NoSuchElementException as ex:
        print("Element username has not been found", ex)
    e.send_keys(username)

    # find and type in password field
    e = driver.find_element_by_id(password_id)
    e.send_keys(password)

    # click on login button
    e = driver.find_element_by_id(login_button_id)
    e.click()

    # wait for 10 sec on second main page for elements to load and for login to pass
    set_timeout(driver, 10)

    # click allow on terms of use & wait for 3 sec
    el1 = driver.find_element_by_xpath(terms_of_use_xpath)
    el1.click()
    set_timeout(driver, 3)

    # click on allow on privacy policy & wait for 3 sec
    el2 = driver.find_element_by_xpath(privacy_policy_xpath)
    el2.click()
    set_timeout(driver, 3)

    # click on Continue button & wait for 3 sec for next page to load elements
    el3 = driver.find_element_by_xpath(first_continue_button_xpath)
    el3.click()
    set_timeout(driver, 3)

    # click allow on location & wait for 3 sec
    ed1 = driver.find_element_by_xpath(location_xpath)
    ed1.click()
    set_timeout(driver, 3)

    # click allow on health data
    ed2 = driver.find_element_by_xpath(health_data_xpath)
    ed2.click()

    # click on health data label, it's also element from which scrolling will happen
    eh1 = driver.find_element_by_xpath(health_data_label_xpath)
    eh1.click()

    # element to scroll to
    eh2 = driver.find_element_by_xpath(element_to_scroll_to_xpath)

    # scroll from element eh1 to eh2
    driver.scroll(eh1, eh2)

    # wait for 3 sec for all page elements to load
    set_timeout(driver, 3)

    # click on email notifications allow button & wait for 3 sec
    ed3 = driver.find_element_by_xpath(notifications_xpath)
    ed3.click()
    set_timeout(driver, 3)

    # click on last element on the page, data usage allow button & wait for 3 sec
    ed4 = driver.find_element_by_xpath(data_usage_xpath)
    ed4.click()
    set_timeout(driver, 3)

    # click on Continue button & wait for 5 sec for next page to load elements
    ed5 = driver.find_element_by_xpath(second_continue_button_xpath)
    ed5.click()
    set_timeout(driver, 3)

    # click on I Get it button & wait for 5 sec for new page to load elements
    eg1 = driver.find_element_by_xpath(i_get_it_button_xpath)
    eg1.click()
    set_timeout(driver, 5)

    # click on skip pairing button & wait for 5 sec for new page to load elements
    ef1 = driver.find_element_by_xpath(skip_pairing_button_xpath)
    ef1.click()
    set_timeout(driver, 3)

    # click on pop-up dopustiti greyp pristipanje polozaju ovog uredaja?
    ei1 = driver.find_element_by_xpath(first_pop_up_xpath)
    ei1.click()
    set_timeout(driver, 5)

    # click on pop-up dopustiti greyp pristipanje fotografijama, medijskim sadrzajima i drugim datotekama na vasem uredaju?
    ej1 = driver.find_element_by_xpath(second_pop_up_xpath)
    ej1.click()
    set_timeout(driver, 5)

    # click on settings button
    ek1 = driver.find_element_by_id(settings_id)
    ek1.click()
    set_timeout(driver, 3)

    # click on personal settings of logged in user
    em1 = driver.find_element_by_xpath(personal_settings_xpath)
    em1.click()
    set_timeout(driver, 3)

    # click on the first name field & wait for 3 sec
    en1 = driver.find_element_by_xpath(first_name_field_xpath)
    en1.click()
    set_timeout(driver, 3)

    # change existing first name of logged in user to value from wanted name variable ("TEST")
    en1.send_keys(wanted_name)
    set_timeout(driver, 3)

    # go back to general settings
    eb1 = driver.find_element_by_id(back_button_id)
    eb1.click()
    set_timeout(driver, 5)

    # get field of name and surname of logged in user after the change in name
    eq1 = driver.find_element_by_id(changed_name_id)

    # get value of name and surname field of logged in user after the change in name
    name = eq1.get_attribute("text")

    # see if test case has passed/failed
    if name == test_name:
        print("test case has passed successfully")
        driver.quit()
    else:
        print("test case has failed!")
        driver.quit()
