# mobile-app-test

-----------------------
Info about application:
-----------------------

This is a test for mobile application using Appium test framework

----------------------
Application contents:
----------------------
1. Config

these are config parameters like all ui elements locators (xpath, id)
It also contains app login details & connection to android configuration

2. Tests

This is an placeholder for test cases, currently it contains only one test case
called "Update personal settings"

---------------------------
Prerequisites to run the app
---------------------------
1. Install and download latest Appium for desktop
2. Run Appium on default uri & port
3. Install python version >= 2.7
4. Download android app (apk) from Play Store
5. Add login credentials for downloaded app in app_credentials.ini
6. Add app's downloaded apk to android_connection.json
7. Connect android phone to your comp via USB
8. Identify device name once connected via USB
9. Update android_connection.json file with your device name from step 8
10. Enable USB debugging on your phone manually
11. Download android studio & if needed adb package too

------------------
How to run the app
------------------
Run the update_account_settings.py file



